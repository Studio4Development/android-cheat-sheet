<p>Activities are really just machines that the Android framework cranks through.  That is, Activities define a whole bunch of "hook" methods that we, as developers, implement.  Then when the right time comes, the Android OS will invoke those "hooks."  The most important of these hook methods are the ones that control the very existence and display of an Activity.  These methods taken together constitute the Activity "Lifecycle."</p>
<p class="warning">Note: As you override these methods to add your own behaviors, don't forget to invoke the super method or you will hit runtime errors almost immediately.</p>
<h2>Lifecycle Methods</h2>
<ol>
	<li>
		<h3>onCreate</h3>
		<p>For a given <strong>instance</strong> of an Activity, onCreate() is executed <strong>once</strong>.  You may have inferred this from the very name - things are typically only created once.  This is where you set your layout and grab references to the various view widgets that you need throughout the activity.</p>
		<p class="warning">Note: Don't do anything too heavy for the processor in onCreate() or you will notice a significant freeze at the beginning of your activity.</p>
		<p>After this: onStart()</p>
	</li>
	<li>
		<h3>onStart</h3>
		<p>Runs <strong>every</strong> time your Activity becomes visible which includes the first after onCreate(), but also after every onRestart() as well.</p>
		<p>Though your Activity is currently visible, there may be various reasons that the user cannot yet interact with it.</p>
		<p>If you need to check for the availability of certain resources (like GPS, or Bluetooth, etc), this is where you do it.  You do it here for 2 reasons.  First, onStart() runs when your Activity starts up so it's a good place to check for resources.  But onStart() also runs every time your Activity comes back from onStop().  A long time can pass between onStop() and onStart(), so by checking for GPS in onStart(), you don't risk that the user went away and then came back lacking GPS and you didn't realize it.</p>
		<p>After this: onResume()</p>
	</li>
	<li>
		<h3>onResume</h3>
		<p>Runs when activity is completely visible and completely usable.</p>
		<p>Re-initialize anything that you disconnected in onPause().</p>
		<p>After this: user does stuff and eventually, onPause()</p>
	</li>
	<li>
		<h3>onRestart</h3>
		<p>Unlike onStart(), onRestart() only gets called when the Activity returns from onStop().</p>
		<p>Use onRestart() to re-build anything you tore down in onStop().</p>
		<p>After this: onStart()</p>
	</li>
	<li>
		<h3>onPause</h3>
		<p>Means that either your Activity is partially obscured (like by a dialog or alert), or it means that your Activity is about to go through the destruction process.  This is where you save lightweight data such as the current state of an email form, but don't save big chunks of data here or you will distort the visual appearance of Activity transitions.  If the visual transition looks choppy anyway, then try moving your data-saving code to onStop() instead.  Additionally, if you have connections to services, or sensors (like GPS), you want to disconnect those elements as well.</p>
		<p>After this: onResume(), or onStop()</p>
	</li>
	<li>
		<h3>onStop</h3>
		<p>When onStop() is called, your activity is guaranteed to no longer be visible.  This is where you want to write all your new heavy data to preferences, or a database, or anywhere else.  Any CPU intensive work that needs to occur before losing your activity should happen here <strong>and not in onPause()</strong>.  In addition, it is suggested that you release any resources that you don't really need to hold onto.  That's because even though your Activity has disappeared, it will stay in memory until Android needs to toss it out.  The more gunk you reference in your activity, the more memory it will take up (don't forget you're developing for phones here).</p>
		<p>After this: onRestart() > onStart(), or onDestroy()</p>
	</li>
	<li>
		<h3>onDestroy</h3>
		<p>This is where you clean out any final references that could lead to a memory leak.  After this, your Activity is completely gone and it's not coming back.</p>
	</li>
</ol>

<h2>Lifecycle Lowdown</h2>
<p>Don't get too worried about all these lifecycle methods.  They are not created equal.  I have constructed many simple Activity's that only override onCreate.  That's it.  As you get more complicated with your application logic, you can start using additional lifecycle callbacks.</p>

<h2>Warning Signs</h2>
<p>If you start getting warnings and errors in Logcat about "leaking" resources, or inexplicable null pointer exceptions, etc. then it's time to take a deeper look at your lifecycle methods.  There is a good chance that you should be releasing resources in onPause() or onStop() and then re-initializing them in onStart(), but you're not, so stuff is getting leaked or null'ed out.</p>