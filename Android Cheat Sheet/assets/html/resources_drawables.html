<p>Drawable resources consist of both bitmap resources (like JPG and PNG images), but also consists of special XML documents that Android can use to draw things.</p>

<p>All drawables go in folders that start with "drawable".  The directories that you probably have in your project include:</p>

<pre>
/res
  /drawable
  /drawable-xhdpi
  /drawable-hdpi
  /drawable-mdpi
  /drawable-ldpi
</pre>

<p>The difference between these directories is discussed below.</p>

<h2>Bitmap Drawables</h2>
<p>Because bitmap images are comprised of pixels, a given image only looks good within a certain range of sizes.  So how does Android deal with your images when there are so many phones of varying sizes?</p>

<p>The answer is that you make different versions of your images corresponding to different sizes of devices and then stick them in different "buckets" for Android to choose from.  Each of the directories above represents one of those "buckets".  The meaning of those directory names are:</p>

<ol>
  <li>xhdpi - eXtra High Dots Per Inch</li>
  <li>hdpi - High Dots Per Inch</li>
  <li>mdpi - Medium Dots Per Inch</li>
  <li>ldpi - Low Dots Per Inch</li>
</ol>

<p>These names are really talking about pixel "densities", or how many pixels are jammed into one inch of screen real estate, but in reality I just pretend that these names are talking about the overall size of the device.  I treat xhdpi like a tablet, hdpi like a large phone, mdpi like a mid-size phone, and ldpi like a phone so small that the owner needs an upgrade.</p>

<p>There are no specific rules about the size of images you put in each bucket.  You'll have to test your images on an assortment of emulators or physical devices to make sure your image sizes are appropriate.  For me, if I'm building a simple app, or a prototype, I'll just make all of my images with hdpi in mind and then let Android scale them down for smaller devices.  It's just <strong>so</strong> much easier that way, albeit sloppy.</p>

<p>The one dimension difference that is worth taking into account is the expected size of the launcher icons - titled "ic_launcher.png" in each of those directories.  The needed sizes are:</p>

<ol>
  <li>xhdpi - 128x128</li>
  <li>hdpi - 72x72</li>
  <li>mdpi - 64x64</li>
  <li>ldpi - 32x32</li>
</ol>

<p>I recommend drawing a new version of your launcher icon for each of those sizes.  Not because Android can't scale down your hdpi version, but because icon details need to change as they get smaller.  This is a part of visual design, not Android development, but think about an icon that has slight highlights and shadows applied to it.  Now scale down that icon by 50%.  All of those highlights and shadows are going to disappear because of how much smaller everything is.  There are similar issues with relative sizing of icon details.  The point is, if you totally recreate your logo at the small sizes, you can adjust these details so they don't get lost in the scaling.</p>

<h3>Cut To The Chase</h3>

<p>If you want the quickest route, create all your images with hdpi in mind, stick them all in the hdpi directory, and Android will just scale the images down for you if a smaller phone uses your app.  It's not the most polished route, but it's certainly the easiest.</p>

<h2>XML Drawables</h2>

<p>XML Drawables are pretty cool.  Android provides a bunch of artifacts that you can declare in XML, like a Shape:</p>

<pre>
&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
&lt;shape xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot; &gt;
  &lt;solid android:color=&quot;#ff8400&quot; /&gt;
  &lt;stroke android:width=&quot;1px&quot; android:color=&quot;#FFFFFF&quot;/&gt;
&lt;/shape&gt;
</pre>

<p>The above XML Drawable is used as the background for each grid item on the home screen of this app.  To use a file like the one above, all you need to do is reference it in another XML file:</p>

<pre>
...
android:background="@drawable/grid_background"
...
</pre>

<p>There are many other XML Drawable artifacts that you can find in the documentation, but they are beyond the scope of this instruction.</p>