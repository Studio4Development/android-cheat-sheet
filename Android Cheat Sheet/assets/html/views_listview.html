<p>ListViews are the most powerful and versatile UI widget given to developers, but it also happens to be one of the more complex widgets too.  That's because ListViews involve a few moving parts.</p>

<p>This information confines itself to data that is represented in a simple data structure, like an Array or a List.  You can also tie ListViews to databases, but you'll need to consult Google for that path.</p>

<h2>The Pieces</h2>

<p>The following are all involved with even the simplest ListView setup:</p>

<ul>
	<li>ListView</li>
	<li>ListAdapter</li>
	<li>Layout for a List Item</li>
	<li>Data for List</li>
</ul>

<h2>The ListView</h2>

<p>The ListView, itself, is responsible for the touch interactions.  That means the ListView handles scrolling of items, it requests new items as they come in from offscreen, and it also recycles list items as they disappear off screen.  This is actually very important to performance - if the ListView didn't recycle list items, it would bring the CPU to a crawl almost immediately.  This is one big reason why you should never try to implement your own version of a ListView using a ScrollView and a LinearLayout.</p>

<p>To actually get list item views to stick in the list, the ListView talks to a ListAdapter.</p>

<h2>The ListAdapter</h2>

<p>A ListAdapter is where the ListView goes to get the visual list items.  The ListAdapter provides list item views by combining a data source with a list item layout.  Think about the process like this:</p>

<ol>
  <li>ListView &gt; ListAdapter: Hey I need the view for the 15th list item</li>
  <li>ListAdapter &gt; ListView: Sure, just a second...</li>
  <li>ListAdapter &gt; Data Source: Hey data, what's your 15th value?</li>
  <li>ListAdapter &gt; List Item Layout: Hey layout, generate another list item view for me</li>
  <li>ListAdapter takes the new list item view and injects the data for the 15th element</li>
  <li>ListAdapter &gt; ListView: Here is a brand new list item view for you to display</li>
  <li>ListView &gt; ListAdapter: Thanks!</li>
</ol>

<p>The above interaction takes place every time a new list item scrolls onto the screen.</p>

<h2>The List Item Layout</h2>

<p>For the layout that applies to each list item, create a new XML layout file and stick whatever you want in there.  It can be as simple as a TextView (like for this app).  You'll just need to write the code in the ListAdapter to map your data item to the TextView.</p>

<h2>The Data Source</h2>

<p>For our simple case we're just talking about using an Array or a List.  You'll write your ListAdapter in a way that accepts an Array or a List and then feed the data as needed within the ListAdapter.</p>

<h2>Example Setup</h2>

<p>All the boring text in the world isn't going to make clear what a single example can.  Included below is most of what it took to setup the lists in this app.</p>

<h3>Setup in Activity's onCreate():</h3>
<pre class="brush:java">
// Create instance of custom ListAdapter
listAdapter = new SubjectListAdapter(this, categoryName);

// Grab a reference to the ListView in the Activity's layout
listView = (ListView)findViewById(R.id.subjects);

// Connect the ListView to the custom ListAdapter
listView.setAdapter(listAdapter);

// Listen for item clicks to do stuff
listView.setOnItemClickListener(new OnItemClickListener() 
{
  @Override
  public void onItemClick(AdapterView<?> listView, View view, int position, long id) 
  {
    // actions to take when an item is clicked.
  }
});
</pre>

<h3>Definition of Custom ListAdapter</h3>
<p>Note: BaseAdapter is a subtype of ListAdapter which provides some free stuff that we don't want to rewrite.</p>
<pre class="brush:java">
public class SubjectListAdapter extends BaseAdapter
{
  private Context context;
  private List<String> subjects;
	
  public SubjectListAdapter(Context context, String categoryName)
  {
    // We need a reference to a context to get a LayoutInflater later
    this.context = context;
    
    // In this particular case we are grabbing a List of Strings
    // as our data from a custom Application object.  You could just
    // as easily pass a List or Array into the constructor.
    subjects = ((CheatSheet)context.getApplicationContext()).getCategoryByName(categoryName);
  }
	
  // Tells the ListView how many items
  // are in the list.  For us that's easy
  // because we're just using a List to hold
  // our data.
  @Override
  public int getCount() {
    return subjects.size();
  }

  // Returns the actual data item corresponding
  // to the provided index - IE the 17'th data item.
  @Override
  public Object getItem(int index) {
    return subjects.get(index);
  }

  // You don't need to understand this method at this
  // point.  You should be able to copy this method over
  // to your adapter as-is.
  @Override
  public long getItemId(int position) {
    return 0;
  }

  // This is the guts of the adapter.  Given an index (aka "position")
  // in the list, return a View that represents the list item.
  //
  // You might wonder why we are getting a View passed in called
  // "convertView".  Remember when I mentioned "recycling"?  If convertView
  // is not Null, then we can re-use it to save on memory.  If it is Null then
  // we have to "inflate" a new version of the list item layout.  Managing the
  // Null check, and the possible layout inflation is what most of this function
  // is doing.
  //
  // Lastly, with a layout in hand, this function just assigns the data
  // item's text to the TextView.
  @Override
  public View getView(int position, View convertView, ViewGroup parent) 
  {
    View updatedView;
		
    if(convertView == null)
    {
      updatedView = (ViewGroup)((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listitem_subject, null);
    }
    else
    {
      updatedView = (ViewGroup) convertView;
    }
		
    ((TextView)updatedView.findViewById(R.id.name)).setText((String)getItem(position));
		
    return updatedView;
  }
}
</pre>

<h3>List Item Layout</h3>
<pre class="brush:java">
&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
  android:layout_width="match_parent"
  android:layout_height="match_parent"
  android:padding="20dp"
  android:background="@drawable/subject_bk"&gt;
    
  &lt;TextView
    android:id="@+id/name"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:layout_alignParentLeft="true"
    android:layout_centerVertical="true"
    android:textSize="20sp"
    /&gt;

&lt;/RelativeLayout&gt;
</pre>

<p>There you have it.  The code above is what makes possible every list of subjects you scroll through and select in this app.  Because the custom ListAdapter runs off any old List<String>, this same code is used for every single ListView you interact with in this app.</p>