package com.cbusa.bootcamp.android;

import android.os.Bundle;
import android.webkit.WebView;

import com.cbusa.bootcamp.android.app.CheatSheet;

public class ContentActivity extends BaseActivity
{
	private WebView webview;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_content);
		
		String subject = getIntent().getExtras().getString("subject");
		CheatSheet app = (CheatSheet)this.getApplication();
		
		webview = (WebView)findViewById(R.id.content);
		webview.loadUrl(app.getContentPageFilepathBySubjectName(subject));
	}
	
}
