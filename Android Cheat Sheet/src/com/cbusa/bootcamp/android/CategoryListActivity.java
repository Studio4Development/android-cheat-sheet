package com.cbusa.bootcamp.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.cbusa.bootcamp.android.adapters.SubjectListAdapter;
import com.cbusa.bootcamp.android.app.CheatSheet;

public class CategoryListActivity extends BaseActivity
{
	private ListView list;
	private ListAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_category_list);
		
		CheatSheet app = (CheatSheet)this.getApplication();
		
		String category = getIntent().getExtras().getString("category");
		
		adapter = new SubjectListAdapter(this, app.getCategoryByName(category));
		list = (ListView)findViewById(R.id.subjects);
		list.setAdapter(adapter);
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> list, View view, int position, long id) {
				Intent intent = new Intent(CategoryListActivity.this, ContentActivity.class);
				intent.putExtra("subject", (String)adapter.getItem(position));
				startActivity(intent);
			}
			
		});
	}
}
