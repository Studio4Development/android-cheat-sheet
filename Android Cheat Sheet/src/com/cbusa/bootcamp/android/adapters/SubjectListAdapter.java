package com.cbusa.bootcamp.android.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cbusa.bootcamp.android.R;

public class SubjectListAdapter extends BaseAdapter
{
	private Context context;
	private List<String> items;

	public SubjectListAdapter(Context context, List<String> items)
	{
		this.context = context;
		this.items = items;
	}
	
	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int index) {
		return items.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null)
		{
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.listitem_subject, null);
		}
		
		TextView content = (TextView)convertView.findViewById(R.id.content);
		content.setText((String)getItem(position));
		
		return convertView;
	}

}
