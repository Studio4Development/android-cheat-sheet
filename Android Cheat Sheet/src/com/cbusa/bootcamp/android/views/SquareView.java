package com.cbusa.bootcamp.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class SquareView extends FrameLayout
{

	public SquareView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	
	@Override
	public void onMeasure(int widthSpec, int heightSpec)
	{
		super.onMeasure(widthSpec, widthSpec);
	}
}
