package com.cbusa.bootcamp.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.cbusa.bootcamp.android.adapters.CategoryGridAdapter;
import com.cbusa.bootcamp.android.app.CheatSheet;

public class HomeActivity extends BaseActivity
{
	private TextView nameDisplay;
	private GridView grid;
	private ListAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_home);
		
		nameDisplay = (TextView)findViewById(R.id.name_display);
		grid = (GridView)findViewById(R.id.categories);
		
		CheatSheet app = (CheatSheet)this.getApplication();
		
		adapter = new CategoryGridAdapter(this, app.getCategoryNames());
		grid.setAdapter(adapter);
		grid.setNumColumns(2);
		
		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> viewContainer, View view, int position, long id) {
				if(position == 0)
				{
					//This is "What Is This App?" and needs to go directly to content view
					Intent intent = new Intent(HomeActivity.this, ContentActivity.class);
					intent.putExtra("subject", (String)adapter.getItem(position));
					startActivity(intent);
					
					
					return;
				}
				
				Intent intent = new Intent(HomeActivity.this, CategoryListActivity.class);
				intent.putExtra("category", (String)adapter.getItem(position));
				startActivity(intent);
			}
			
		});
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		SharedPreferences prefs = this.getSharedPreferences("cheat_sheet", Context.MODE_PRIVATE);
		String username = prefs.getString("name", "");
		nameDisplay.setText(username);
	}
	
}
