package com.cbusa.bootcamp.android.app;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Application;

public class CheatSheet extends Application
{
	private String[] categoryNames;
	private Map<String, String[]> categories;
	private Map<String, String> subjectContentMap;
	
	public CheatSheet()
	{
		categoryNames = new String[] {
			"What Is This App?",
			"Activities",
			"Views",
			"Resources",
			"Menus",
			"Shared Prefs"
		};
		
		categories = new HashMap<String, String[]>();
		categories.put("What Is This App?", new String[] {});
		categories.put("Activities", new String[] {
			"Lifecycle", "Using Layouts", "Intents"
		});
		categories.put("Views", new String[] {
			"FrameLayout", "LinearLayout", "RelativeLayout", "ScrollView", "ListView", "GridView"
		});
		categories.put("Shared Prefs", new String[] {
			"Read Preferences", "Write Preferences", "Permissions"
		});
		categories.put("Menus", new String[] {
			"XML Definitions", "Using in Activities"
		});
		categories.put("Resources", new String[] {
			"Layouts", "Drawables", "Styles", "Strings"
		});
		
		subjectContentMap = new HashMap<String, String>();
		
		// About Page
		subjectContentMap.put("What Is This App?", "about_this_app.html");
		
		// Activities
		subjectContentMap.put("Lifecycle", "activity_lifecycle.html");
		subjectContentMap.put("Using Layouts", "activity_using_layouts.html");
		subjectContentMap.put("Intents", "activity_intents.html");
		
		// Views
		subjectContentMap.put("FrameLayout", "views_framelayout.html");
		subjectContentMap.put("LinearLayout", "views_linearlayout.html");
		subjectContentMap.put("RelativeLayout", "views_relativelayout.html");
		subjectContentMap.put("ScrollView", "views_scrollview.html");
		subjectContentMap.put("ListView", "views_listview.html");
		subjectContentMap.put("GridView", "views_gridview.html");
		
		// Shared Prefs
		subjectContentMap.put("Read Preferences", "sharedprefs_read.html");
		subjectContentMap.put("Write Preferences", "sharedprefs_write.html");
		subjectContentMap.put("Permissions", "sharedprefs_permissions.html");
		
		// Menus
		subjectContentMap.put("XML Definitions", "menus_xml.html");
		subjectContentMap.put("Using in Activities", "menus_usage.html");
		
		// Resources
		subjectContentMap.put("Layouts", "resources_layouts.html");
		subjectContentMap.put("Drawables","resources_drawables.html");
		subjectContentMap.put("Styles", "resources_styles.html");
		subjectContentMap.put("Strings", "resources_strings.html");
	}
	
	public List<String> getCategoryNames() 
	{
		return Arrays.asList( categoryNames );
	}
	
	public List<String> getCategoryByName(String name)
	{
		if(categories.containsKey(name))
		{
			return Arrays.asList( categories.get(name) );
		}
		
		return null;
	}
	
	public String getContentPageFilepathBySubjectName(String subjectName)
	{
		String PREFIX = "file:///android_asset/html/";
		
		return PREFIX + subjectContentMap.get(subjectName);
	}
	
	public String getContentPageFilenameBySubjectName(String subjectName)
	{
		return subjectContentMap.get(subjectName);
	}
}
