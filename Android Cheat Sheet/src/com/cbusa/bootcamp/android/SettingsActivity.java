package com.cbusa.bootcamp.android;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SettingsActivity extends BaseActivity
{
	private EditText nameInput;
	private Button saveButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState, R.layout.activity_settings);
		
		nameInput = (EditText)findViewById(R.id.name_input);
		saveButton = (Button)findViewById(R.id.save_button);
		
		saveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				saveName();
			}
			
		});
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		nameInput.setText(this.getSharedPreferences("cheat_sheet", Context.MODE_PRIVATE).getString("name", ""));
	}
	
	private void saveName()
	{
		String newName = nameInput.getText().toString();
		
		SharedPreferences.Editor editor = this.getSharedPreferences("cheat_sheet", Context.MODE_PRIVATE).edit();
		editor.putString("name", newName);
		editor.commit();
	}
	
}
